https://www.upwork.com/jobs/Script-extract-all-emails-from-data-file_%7E01eaf88a7e3f8f77b5

I am looking for a script to extract/scrape all the valid emails from a file. It has to do the following functions:

The script should convert all the emails extracted to lower case and create a file with 1 email on each line and a summary report at the end. with no blank spaces before or after each email. At the bottom of the file it should give a list of all the domains that have more than 10 emails. At the bottom of the list there should be a report section.

The script should be perl or shell script, or awk or sed or something common, taht I don't mneed to install on my mac.


The script should ignore emails that : 
1. are duplicate (1 email should be captured only once)
2. are longer than 40 characters
3. contains 5 numbers or more together: e,g a11225.john@gmail.com or john@a11225b.com
4. do not have the domain .com, .org, .gov, .info, .gov, .ca, .mx, .in, .tv, .ly and .edu
5. start with a digit. e.g. 0john@gmail.com, 00002@gmail.com
6. that contains the word: "reply, support, help, sales, marketing, hotmail, etc" (This list of words to exclude could be upto 100 words. - All theses words should be defined in the script itself)

Notes: 
1. The script should accept 1 parameter during run time: "File_name". This is the data file name that contains all the emails taht need to be extracted.

extract.sh data-file

2. The name of the file produced will have the name output.txt. (This file will contain 



PART B:

The report section at the end should give the following summary (Sample):

--------------------------------
Total unique emails captured: 1232

Unique Emails by domain:
gmail.com :132
msn.com : 75
oracle.com:43
yahoo.com : 19
mail.com:11
--------------------------------

Note: If any domain, has less than 10 unique emails, we dont need to have that in the report.